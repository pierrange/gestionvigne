
<!-- ----- debut Router2 -->
<?php
require ('../controller/ControllerProducteur.php');
require ('../controller/ControllerVin.php');
require ('../controller/ControllerProjet.php');


// --- récupération de l'action passée dans l'URL
$query_string = $_SERVER['QUERY_STRING'];

// fonction parse_str permet de construire 
// une table de hachage (clé + valeur)
parse_str($query_string, $param);

// --- $action contient le nom de la méthode statique recherchée
$action = htmlspecialchars($param["action"]);


// --- Modification du routeur pour prendre en compte les paramètres
$action = $param['action'];

//-- On supprime l'élément action de la structure
unset($param["action"]);

//-- Tout ce qui reste sont des arguments
$args = $param;


// --- Liste des méthodes autorisées
switch ($action) {
    case "vinReadAll" :
        ControllerVin::$action($args);
        break;
    case "vinReadOne" :
        ControllerVin::$action($args);
        break;
    case "vinReadId" :
        ControllerVin::$action($args);
        break;
    case "vinDeleted" :
        ControllerVin::$action($args);
        break;

    case "vinCreate" :
        ControllerVin::$action($args);
        break;
    case "vinCreated" :
        ControllerVin::$action($args);
        break;
    case "producteurReadAll" :
        ControllerProducteur::$action($args);
        break;
    case "producteurReadOne" :
        ControllerProducteur::$action($args);
        break;
    case "producteurReadId" :
        ControllerProducteur::$action($args);
        break;
    case "producteurCreate" :
        ControllerProducteur::$action($args);
        break;
    case "producteurCreated" :
        ControllerProducteur::$action($args);
        break;
    case "producteurDeleted" :
        ControllerProducteur::$action($args);
        break;
    case "producteurRegion" :
        ControllerProducteur::$action($args);
        break;
    case "Document" :
        ControllerProjet::$action($args);
        break;
    case "projetDoc" :
        ControllerProjet::$action($args);
        break;
    case "projetReadProducteur" :
        ControllerProjet::$action($args);
        break;
    case "projetReadOneProducteur" :
        ControllerProjet::$action($args);
        break;
    case "projetResultSeuil" :
        ControllerProjet::$action($args);
        break;
    case "projetReadSeuil" :
        ControllerProjet::$action($args);
        break;
    case "projetReadRegion" :
        ControllerProjet::$action($args);
        break;
    case "projetResultRegion" :
        ControllerProjet::$action($args);
        break;
    case "projetReadVin" :
        ControllerProjet::$action($args);
        break;
    case "projetLabelisation" :
        ControllerProjet::$action($args);
        break;
    case "projetFiltrer" :
        ControllerProjet::$action($args);
        break;
    case "projetResultFiltre" :
        ControllerProjet::$action($args);
        break;
    // Tache par défaut
    default:
        $action = "caveAccueil";
        ControllerVin::$action();
}
?>
<!-- ----- Fin Router2 -->

