
<!-- ----- début viewInsert -->
 
<?php 
require ($root . '/app/view/fragment/fragmentCaveHeader.html');
?>

<body>
  <div class="container">
    <?php
      include $root . '/app/view/fragment/fragmentCaveMenu.html';
      include $root . '/app/view/fragment/fragmentCaveJumbotron.html';
    ?> 

    <form role="form" method='get' action='router2.php'>
      <div class="form-group">
        <input type="hidden" name='action' value='vinCreated'>        
        <label for="id">Cru : </label><input type="text" name='cru' size='75' value='Champagne de déconfinement' required>                           
        <label for="id">Annee : </label><input type="number" name='annee' value='2021'required>
        <label for="id">Degre : </label><input type="text" name='degre' value='17.24' required>       
        
      </div>
      <p/>
      <button class="btn btn-primary" type="submit">Insérer ce vin</button>
    </form>
    <p/>
  </div>
  <?php include $root . '/app/view/fragment/fragmentCaveFooter.html'; ?>

<!-- ----- fin viewInsert -->



