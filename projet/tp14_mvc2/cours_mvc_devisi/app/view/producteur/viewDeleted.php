
<!-- ----- début viewDelete -->
<?php
require ($root . '/app/view/fragment/fragmentCaveHeader.html');
?>

<body>
  <div class="container">
    <?php
    include $root . '/app/view/fragment/fragmentCaveMenu.html';
    include $root . '/app/view/fragment/fragmentCaveJumbotron.html';
    ?>
    <!-- ===================================================== -->
    <?php
             if($results==2)
                 echo "<p>L'élément $producteur_id a bien été supprimé</p>"; 
             else if($results==1)
                 echo "<p>Un problème est survenu, Il est probable que l'élément $producteur_id soit dans la table récolte</p>";
             else
                 echo "<p>Un problème est survenu, l'élément $producteur_id n'a pas pu être supprimé</p>";

    
    include $root . '/app/view/fragment/fragmentCaveFooter.html';
    ?>
    <!-- ----- fin viewDelete-->    

    
    