
<!-- ----- début viewId -->
<?php
require ($root . '/app/view/fragment/fragmentCaveHeader.html');
?>

<body>
    <div class="container">
        <?php
        include $root . '/app/view/fragment/fragmentCaveMenu.html';
        include $root . '/app/view/fragment/fragmentCaveJumbotron.html';

        // $results contient un tableau avec la liste des clés.
        ?>

        <form role="form" method='get' action='router2.php'>
            <div class="form-group">
                <input type="hidden" name='action' value='projetResultRegion'>
                <label for="region">Nom de la Région :</label> <select class="form-control" region='region' name='region' style="width: 150px">
                    <?php
                    while ($donnee = $results->fetch()) {
                        $region=$donnee['region'];
//                        $nom = $donnee['nom'];
//                        $prenom = $donnee['prenom'];
//                        $producteur_id=$donnee['id'];
                        echo ("<option value='region'>$region</option>");
                    }
                    ?>
                </select>
            </div>
            <p/>
            <button class="btn btn-primary" type="submit">Choisir cette Région</button>
        </form>
        <p/>
    </div>

    <?php include $root . '/app/view/fragment/fragmentCaveFooter.html'; ?>

    <!-- ----- fin viewId -->