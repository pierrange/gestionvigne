
<!-- ----- début viewProjetFilter -->

<?php
require ($root . '/app/view/fragment/fragmentCaveHeader.html');
?>

<body>
    <div class="container">
        <?php
        include $root . '/app/view/fragment/fragmentCaveMenu.html';
        include $root . '/app/view/fragment/fragmentCaveJumbotron.html';
        ?> 

        <form role="form" method='get' action='router2.php'>
            <div class="form-group">
                <input type="hidden" name='action' value='projetResultFiltre'>        
                <label for="seuil">Dégré Minimal <input type="number" name='degre' size='10' value='12' required> </label>                         
                <label for="annee">Année <input type="number" name='annee' size='10' value='1980' required> </label>
                <label for="quantite">Quantié Minimale<input type="number" name='quantite' size='10'  value="5" required> </label>
                <label>AOP?</label>
                <select name="aop" value="false" required>
                    <option value="false">Non</option>
                    <option value="true">Oui</option>
                    
                </select>
            </div>
            <p/>
            <button class="btn btn-primary" type="submit">Filtrer</button>
        </form>
        <p/>
    </div>
    <?php include $root . '/app/view/fragment/fragmentCaveFooter.html'; ?>

    <!-- ----- fin viewProjetFilter -->



