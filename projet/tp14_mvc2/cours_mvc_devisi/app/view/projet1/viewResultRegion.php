
<!-- ----- début viewResultRegion -->
<?php
require ($root . '/app/view/fragment/fragmentCaveHeader.html');
?>

<body>
    <div class="container">
        <?php
        include $root . '/app/view/fragment/fragmentCaveMenu.html';
        include $root . '/app/view/fragment/fragmentCaveJumbotron.html';
        ?>

        <table class = "table table-striped table-bordered">
            <thead>
                <tr>
                    <th scope = "col">Cru</th>
                    <th scope = "col">Année</th>
                    <th scope = "col">Dégré</th>
                    <th scope = "col">Quantité</th>
                     <th scope = "col">Producteur</th>
                </tr>
            </thead>
            <tbody>
                <?php
                // La liste des vins est dans une variable $results                                           
                while ($donnees = $results->fetch()) {
                    ?>
                    <tr>
                        <td><?php echo $donnees['cru']; ?></td>
                        <td><?php echo $donnees['annee']; ?></td>
                        <td><?php echo $donnees['degre']; ?></td>
                        <td><?php echo $donnees['quantite']; ?></td>
                        <td><?php echo $donnees['nom']."  " .$donnees['prenom']; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <?php include $root . '/app/view/fragment/fragmentCaveFooter.html'; ?>

    <!-- ----- fin viewResultRegion -->