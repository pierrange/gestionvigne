
<!-- ----- début viewId -->
<?php
require ($root . '/app/view/fragment/fragmentCaveHeader.html');
?>

<body>
    <div class="container">
        <?php
        include $root . '/app/view/fragment/fragmentCaveMenu.html';
        include $root . '/app/view/fragment/fragmentCaveJumbotron.html';

        // $results contient un tableau avec la liste des clés.
        ?>

        <form role="form" method='get' action='router2.php'>
            <div class="form-group">
                <input type="hidden" name='action' value='projetReadOneProducteur'>
                <label for="id">Nom et Prenom du Producteur </label> <select class="form-control" id='id' name='id' style="width: 200px">
                    <?php
                    while ($donnee = $results->fetch()) {
                        $nom = $donnee['nom'];
                        $prenom = $donnee['prenom'];
                        $producteur_id=$donnee['id'];
                        echo ("<option value='producteur_id' >$nom $prenom</option>");
                    }
                    ?>
                </select>
            </div>
            <p/>
            <button class="btn btn-primary" type="submit">Choisir ce producteur</button>
        </form>
        <p/>
    </div>

    <?php include $root . '/app/view/fragment/fragmentCaveFooter.html'; ?>

    <!-- ----- fin viewId -->