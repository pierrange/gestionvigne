
<!-- ----- début viewId -->
<?php
require ($root . '/app/view/fragment/fragmentCaveHeader.html');
?>

<body>
    <div class="container">
        <?php
        include $root . '/app/view/fragment/fragmentCaveMenu.html';
        include $root . '/app/view/fragment/fragmentCaveJumbotron.html';

        // $results contient un tableau avec la liste des clés.
        ?>

        <form role="form" method='get' action='router2.php'>
            <div class="form-group">
               
                <input type="hidden" name='action' value='projetLabelisation'>
                <label for="vin_id">Cru : </label> <select class="form-control" vin_id='vin_id' name='vin_id' style="width: 200px">

                    <?php
                    $cru;
                    $prenom;
                    $nom;
                    while ($donnee = $results->fetch()) {

                        $cru = $donnee['cru'];
                        $prenom = $donnee['prenom'];
                        $nom = $donnee['nom'];
                        $vin_id = $donnee['vin_id'];
                       echo ("<option value=$vin_id >$cru du Producteur  $nom $prenom</option>");
                    }
                    ?>
                </select>

            </div>
            <p/>
            <button class="btn btn-primary" type="submit">Choisir ce cru</button>
        </form>
        <p/>
    </div>

<?php include $root . '/app/view/fragment/fragmentCaveFooter.html'; ?>

    <!-- ----- fin viewId 