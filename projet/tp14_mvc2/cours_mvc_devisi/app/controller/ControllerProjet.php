
<!-- ----- debut ControllerProjet -->
<?php
require_once '../model/ModelProjet.php';

class ControllerProjet {

    // --- page d'acceuil
    public static function caveAccueil() {
        include 'config.php';
        $vue = $root . '/app/view/viewCaveAccueil.html';

        require ($vue);
    }

    // --- Liste des vins
    public static function vinReadAll() {
        $results = ModelProjet::getAll();
        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/vin/viewAll.php';
        require ($vue);
    }

    // Affiche un formulaire pour sélectionner un producteur par son nom et prénom
    public static function projetReadProducteur() {

        $results = ModelProjet::getAllProducteur_id();
//  $target=$args['target'];
        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/projet/viewprojetReadProducteur.php';
        require ($vue);
    }

    //affice les vins produits par un producteur
    public static function projetReadOneProducteur() {
        $producteur_id = $_GET['id'];
        $results = ModelProjet::getOneProducteur($producteur_id);

        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/projet/viewProducteurVin.php';
        require ($vue);
    }

    //affiche une zone de saisie pour choisir le dégré seuil
    public static function projetReadSeuil() {
// 
//  $target=$args['target'];
        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/projet/viewProjetReadSeuil.php';
        require ($vue);
    }

    //affiche tous les vins dont le degre est inférieur au seuil
    public static function projetResultSeuil() {
        $seuil = $_GET['seuil'];

        $results = ModelProjet::getResultSeuil($seuil);
        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/projet/viewResultSeuil.php';
        require ($vue);
    }

    //filtrer les vins 
    public static function projetFiltrer() {
// 
//  $results = ModelProjet::getVinFiltre();
//  $target=$args['target'];
        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/projet/viewProjetFiltrer.php';
        require ($vue);
    }

    //resultat du filtre
    public static function projetResultFiltre() {
        $degre = $_GET['degre'];
        $annee = $_GET['annee'];
        $aop = $_GET['aop'];
        $quantite = $_GET['quantite']; //qunatité min
        $results = ModelProjet::getResultFiltre($degre, $annee, $aop, $quantite);

        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/projet/viewResultFiltre.php';
        require ($vue);
    }

    //affiche un formulaire pour choisir une région
    public static function projetReadRegion() {
// 
        $results = ModelProjet::getAllRegion();
//  $target=$args['target'];
        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/projet/viewRegion.php';
        require ($vue);
    }

    //affice les vins produits dans une region
    public static function projetResultRegion() {
        $region = $_GET['region'];
        $results = ModelProjet::getOneRegion($region);

        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/projet/viewResultRegion.php';
        require ($vue);
    }

    //
    public static function projetReadVin() {
        $results = ModelProjet::getAllVin();
//  $target=$args['target'];
        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/projet/viewProjetReadVin.php';
        require ($vue);
    }

    public static function projetLabelisation() {
  $vin_id=$_GET['vin_id'];
//  $nom=$_GET['vin_id'][1];
//  $prenom=$_GET['vin_id'][2];
//  $cru=$_GET['vin_id'][3];
        $results = ModelProjet::labeliserOneVin($vin_id);

        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/projet/viewLabeliserVin.php';
        require ($vue);
    }

//documentation1
    public static function Document() {

        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/public/documentation/mesPropositions.php';
        require ($vue);
    }

//mon projet
    public static function projetDoc() {

        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/public/documentation/monProjet.php';
        require ($vue);
    }

    public static function vinDeleted($args) {

        $id = $_GET['id'];
        $results = ModelProjet::delete($id);
        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/vin/viewDelete.php';
        require ($vue);
    }

    // Affiche un vin particulier (id)
    public static function vinReadOne() {
        $vin_id = $_GET['id'];
        $results = ModelProjet::getOne($vin_id);

        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/vin/viewAll.php';
        require ($vue);
    }

    // Affiche le formulaire de creation d'un vin
    public static function vinCreate() {
        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/vin/viewInsert.php';
        require ($vue);
    }

    // Affiche un formulaire pour récupérer les informations d'un nouveau vin.
    // La clé est gérée par le systeme et pas par l'internaute
    public static function vinCreated() {
        // ajouter une validation des informations du formulaire
        $results = ModelProjet::insert(
                        htmlspecialchars($_GET['cru']), htmlspecialchars($_GET['annee']), htmlspecialchars($_GET['degre'])
        );
        // ----- Construction chemin de la vue
        include 'config.php';
        $vue = $root . '/app/view/vin/viewInserted.php';
        require ($vue);
    }

}
?>
<!-- ----- fin ControllerProjet -->


