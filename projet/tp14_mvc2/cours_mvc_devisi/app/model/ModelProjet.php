
<!-- ----- debut ModelProjet -->
<?php
require_once 'Model.php';

class ModelProjet {

 private $producteur_id, $vin_id, $quantite;

 // pas possible d'avoir 2 constructeurs
 public function __construct($producteur_id = NULL, $vin_id = NULL, $quantite = NULL) {
  // valeurs nulles si pas de passage de parametres
  if (!is_null($producteur_id)) {
   $this->producteur_id = $producteur_id;
   $this->vin_id = $vin_id;
   $this->quantite = $quantite;
   
  }
 }

 function setProducteur_id($producteur_id) {
  $this->producteur_id = $producteur_id;
 }

 function setVin_id($vin_id) {
  $this->vin_id = $vin_id;
 }

 function setQuantite($quantite) {
  $this->quantite = $quantite;
 }

 
 function getProducteur_id() {
  return $this->producteur_id;
 }

 function getVin_id() {
  return $this->vin_id;
 }

 function getQuantite() {
  return $this->quantite;
 }


 public function __toString() {
    return $this->producteur_id;
  
 }

 // Persistance .......


 public static function view() {
  printf("<tr><td>%d</td><td>%s</td><td>%d</td><td>%.00f</td></tr>", $this->getProducteur_id(), $this->getVin_id(), $this->getQuantite(), $this->getDegre());
 }

// retourne une liste des producteur_id
 public static function getAllProducteur_id() {
  try {
   $database = Model::getInstance();
   $query = "select id,nom,prenom from producteur order by nom, prenom";
   $statement = $database->prepare($query);
   $statement->execute();
   //$results = $statement->fetchAll(PDO::FETCH_COLUMN, 0);
   return $statement;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }
public static function getOneProducteur($producteur_id) {
  try {
   $database = Model::getInstance();
   $query = "select cru,quantite,annee,degre from recolte,vin,producteur where vin.id=vin_id AND producteur.id=producteur_id AND producteur_id=$producteur_id group by cru";
   $statement = $database->prepare($query);
   $statement->execute([
     'producteur_id' => $producteur_id
   ]);
//   $results = $statement->fetchAll(PDO::FETCH_CLASS, "ModelProjet");
   return $statement;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }
 
 
 public static function getResultSeuil($seuil) {
  try {
   $database = Model::getInstance();
   $query = "select cru,quantite,annee,degre from recolte,vin,producteur where vin.id=vin_id AND producteur.id=producteur_id AND degre<=:seuil  group by cru order by degre";
   $statement = $database->prepare($query);
   $statement->execute([
       'seuil' => $seuil
           ]);
//   $results = $statement->fetchAll(PDO::FETCH_CLASS, "ModelProjet");
   
   return $statement;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }
 //
 public static function getResultFiltre($degre,$annee,$aop,$quantite) {
  try {
   $database = Model::getInstance();
   $query = "select cru,quantite,annee,degre,AOP,nom,prenom from recolte,vin,producteur where vin.id=vin_id AND producteur.id=producteur_id AND quantite>=$quantite AND degre>=$degre AND annee=$annee AND AOP=$aop  group by cru order by cru";
   $statement = $database->prepare($query);
   $statement->execute();
//   $results = $statement->fetchAll(PDO::FETCH_CLASS, "ModelProjet");
   
   return $statement;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }
 public static function getAllRegion() {
  try {
   $database = Model::getInstance();
   $query = "select distinct region from producteur order by region";
   $statement = $database->prepare($query);
   $statement->execute();
   //$results = $statement->fetchAll(PDO::FETCH_COLUMN, 0);
   return $statement;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }
public static function getOneRegion($region) {
  try {
   $database = Model::getInstance();
   $query = "select cru,quantite,annee,degre,nom,prenom from recolte,vin,producteur where vin.id=vin_id AND producteur.id=producteur_id AND region=$region group by cru";
   $statement = $database->prepare($query);
   $statement->execute([
     'region' => $region
   ]);
//   $results = $statement->fetchAll(PDO::FETCH_CLASS, "ModelProjet");
   return $statement;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }
 //liste des vins et des producteurs associés
  public static function getAllVin() {
 try {
   $database = Model::getInstance();
   $query = "select vin_id,cru,nom,prenom from vin,producteur,recolte where vin.id=vin_id AND producteur.id=producteur_id group by cru";
   $statement = $database->prepare($query);
   $statement->execute();
   //$results = $statement->fetchAll(PDO::FETCH_CLASS, "ModelVin");
   return $statement;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }
 //labeliser un vin
 public static function labeliserOneVin($vin_id){
 try {
   $database = Model::getInstance();
   
   $query = "UPDATE vin,recolte,producteur SET AOP = true WHERE vin_id=$vin_id AND vin.id=vin_id AND producteur.id=producteur_id";
   $statement = $database->prepare($query);
   $statement->execute();
   //$results = $statement->fetchAll(PDO::FETCH_CLASS, "ModelVin");
   return $statement;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }
//filtrer es vins
 
 public static function getMany($query) {
  try {
   $database = Model::getInstance();
   $statement = $database->prepare($query);
   $statement->execute();
   $results = $statement->fetchAll(PDO::FETCH_CLASS, "ModelProjet");
   return $results;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }

 public static function getAll() {
  try {
   $database = Model::getInstance();
   $query = "select * from recolte";
   $statement = $database->prepare($query);
   $statement->execute();
   $results = $statement->fetchAll(PDO::FETCH_CLASS, "ModelProjet");
   return $results;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }

 

 
 public static function insert($vin_id, $quantite) {
  try {
   $database = Model::getInstance();

   // recherche de la valeur de la clé = max(producteur_id) + 1
   $query = "select max(producteur_id) from recolte";
   $statement = $database->query($query);
   $tuple = $statement->fetch();
   $producteur_id = $tuple['0'];
   $producteur_id++;

   // ajout d'un nouveau tuple;
   $query = "insert into recolte value (:producteur_id, :vin_id, :quantite, :degre)";
   $statement = $database->prepare($query);
   $statement->execute([
     'producteur_id' => $producteur_id,
     'vin_id' => $vin_id,
     'quantite' => $quantite,
     'degre' => $degre
   ]);
   return $producteur_id;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return -1;
  }
 }
 
 

 public static function update() {
  echo ("ModelProjet : update() TODO ....");
  return null;
 }

 public static function delete($producteur_id) {
  try {
   $database = Model::getInstance();
   $query = "delete from recolte where producteur_id=$producteur_id and producteur_id not in (select recolte_producteur_id from recolte)";
   $statement = $database->exec($query);
   
   // test existence du recolte
   $query = "select producteur_id from recolte where producteur_id=$producteur_id";
   $statement = $database->query($query);
   $tuple=$statement->fetch() ;
   if ($tuple['producteur_id']!=null){
      return " <strong>Problème de suppression du recolte. Il est probable qu'il soit présent dans la table des récoltes </strong>";  
   }else{
        return " <strong>Le recolte a été supprimé avec succès</strong>";
   }
   
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage()) ;
    return " <strong>Problème de suppression. Le recolte n'a pas pu être supprimé </strong>";
  }
 
 }

}
?>
<!-- ----- fin ModelProjet -->
