
<!-- ----- début viewDocument -->
<?php
require ($root . '/app/view/fragment/fragmentCaveHeader.html');
?>

<body>
    <div class="container">
        <?php
        include $root . '/app/view/fragment/fragmentCaveMenu.html';
        include $root . '/app/view/fragment/fragmentCaveJumbotron.html';
        ?> 
        <h4>Alors mon projet me satisfait car:</h4>
        <ol>
            <li><h4>Il permet d'afficher tous les vins issu d'un producteur donné, non pas à partir de son id car très peu intuitif, mais à partir de son nom et prenom</h4></li>
            <hr/>
            <li><h4>Donne la possibilité à un utilisateur (client) de choisir des vins en fonctions de sa tolérance à l'alcool en affichant des vins ayant un dégré d'alcool sous un seuil maximum(PS: 70° c'est le must ;))</h4></li>
            <hr/>
            <li><h4>En rapport avec l'actualité (hors corona!) et les problèmes d'authenticité de certains vin,mon site donne la possibilté à une autorité de certifier un vin produit par un tel. En effet un attribut AOP :Appelation d'Origine Protégée a été ajouté pour labeliser des productions conformes aux normes et au appelations des crus! </h4></li>
            <hr/>
            <li> <h4>Il permet de filtrer en quelque sorte les vins en fonctions de ses préferences. Cette fonctionnalité est u  filtre sur le dégré minimal, la quantié minimale (pour un client par exemple),le label ou pas ainsi que l'année de production (c'est la vielle marmite qui fait la bonne sauce...) </h4></li>
            <hr/>
            <li><h4>Le site donne la possibilité d'afficher les vins d'une région particulière : un vin de l'Alsace n'est rien devant un fabuleux Vin de Troyes(Grand Est)</h4></li>
            <hr/>
        </ol>
    </div>
    <?php include $root . '/app/view/fragment/fragmentCaveFooter.html'; ?>

    <!-- ----- fin viewDocument -->