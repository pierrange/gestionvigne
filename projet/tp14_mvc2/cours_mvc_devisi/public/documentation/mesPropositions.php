
<!-- ----- début viewDocument -->
<?php
require ($root . '/app/view/fragment/fragmentCaveHeader.html');
?>

<body>
    <div class="container">
        <?php
        include $root . '/app/view/fragment/fragmentCaveMenu.html';
        include $root . '/app/view/fragment/fragmentCaveJumbotron.html';
        ?> 
        <h4>Propositions d'amélioration du Modèle MVC de LO07 </h4>
        <ol>
            <li><h4>Rajouter un contrôle de saisie pour par exemple empêcher l'utilisateur de rentrer un année qui n'est pas un nombre (input type="number")</h4></li>
            <hr/>

            <li><h4>Il est aussi préférable de préciser que tous les champs sont réquis pour l'insertiion d'un vin par exemple</h4> </li>
            <hr/>

        </ol>
    </div>
    <?php include $root . '/app/view/fragment/fragmentCaveFooter.html'; ?>

    <!-- ----- fin viewDocument -->