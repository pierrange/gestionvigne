
<!-- ----- debut Router1 -->
<?php
require ('../controller/ControllerVin.php');
require ('../controller/ControllerProducteur.php');

// --- récupération de l'action passée dans l'URL
$query_string = $_SERVER['QUERY_STRING'];

// fonction parse_str permet de construire 
// une table de hachage (clé + valeur)
parse_str($query_string, $param);

// --- $action contient le nom de la méthode statique recherchée
$action = htmlspecialchars($param["action"]);

// --- Liste des méthodes autorisées
switch ($action) {
    case "vinReadAll" :
            ControllerVin::$action();
    break;
    case "vinReadOne" :
        ControllerVin::$action();
        break;
    case "vinReadId" :
        ControllerVin::$action();
        break;
    case "vinCreate" :
        ControllerVin::$action();
        break;
    case "vinCreated" :
        ControllerVin::$action();
        break;
    case "producteurReadAll" :
        ControllerProducteur::$action();
        break;
    case "producteurReadOne" :
        ControllerProducteur::$action();
        break;
    case "producteurReadId" :
        ControllerProducteur::$action();
        break;
    case "producteurCreate" :
        ControllerProducteur::$action();
        break;
    case "producteurCreated" :
        ControllerProducteur::$action();
        break;
    case "producteurRegion" :
        ControllerProducteur::$action();
        break;
    case "documentation" :
        ControllerProducteur::$action();
        break;
    case "producteur_Vin" :
        ControllerRecolte::$action();
        
    // Tache par défaut
    default:
        $action = "caveAccueil";
        ControllerVin::$action();
//        ControllerProducteur::$action();
}
?>
<!-- ----- Fin Router1 -->

