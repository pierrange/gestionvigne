
<!-- ----- debut ControllerRecokte -->
<?php
require_once '../model/ModelRecolte.php';

class ControllerRecolte {
 // --- page d'acceuil
 public static function caveAccueil() {
  include 'config.php';
  $vue = $root . '/app/view/viewCaveAccueil.html';
  if (DEBUG==true)
   echo ("ControllerRecolte : caveAccueil : vue = $vue");
  require ($vue);
 }

 // --- Tous les vins d'un producteur
 public static function producteur_Vin() {
  $results = ModelRecolte::getProducteur_vin();
  // ----- Construction chemin de la vue
  include 'config.php';
  $vue = $root . '/app/view/recolte/viewproducteur_Vin.php';
  if (DEBUG==true)
   echo ("ControllerRecolte :producteur_Vin : vue = $vue");
  require ($vue);
 }

 // Affiche un formulaire pour sélectionner un id qui existe
 public static function recolteReadId() {
  $results = ModelRecolte::getAllId();

  // ----- Construction chemin de la vue
  include 'config.php';
  $vue = $root . '/app/view/recolte/viewId.php';
  require ($vue);
 }

 // Affiche un recolte particulier (id)
 public static function recolteReadOne() {
  $recolte_id = $_GET['id'];
  $results = ModelRecolte::getOne($recolte_id);

  // ----- Construction chemin de la vue
  include 'config.php';
  $vue = $root . '/app/view/recolte/viewAll.php';
  require ($vue);
 }

 // Affiche le formulaire de creation d'un recolte
 public static function recolteCreate() {
  // ----- Construction chemin de la vue
  include 'config.php';
  $vue = $root . '/app/view/recolte/viewInsert.php';
  require ($vue);
 }

 // Affiche un formulaire pour récupérer les informations d'un nouveau recolte.
 // La clé est gérée par le systeme et pas par l'internaute
 public static function recolteCreated() {
  // ajouter une validation des informations du formulaire
  $results = ModelRecolte::insert(
      htmlspecialchars($_GET['cru']), htmlspecialchars($_GET['annee']), htmlspecialchars($_GET['degre'])
  );
  // ----- Construction chemin de la vue
  include 'config.php';
  $vue = $root . '/app/view/recolte/viewInserted.php';
  require ($vue);
 }
 
}
?>
<!-- ----- fin ControllerRecolte -->


