<!-- ----- début viewId -->
<?php 
include_once ('../controller/config.php');
require ($root . '/app/view/fragment/fragmentCaveHeader.html');
?>

<body>
  <div class="container">
      <?php
      include $root . '/app/view/fragment/fragmentCaveMenu.html';
      include $root . '/app/view/fragment/fragmentCaveJumbotron.html';

      ?>

    <div class='panel panel-success'>
          <div class='panel-heading'>Suppression de l'élément donc l'id est <?php echo $vin_id; ?></div>
          <div class='panel-body'>
             <?php 
             if($results==2)
                 echo "<p>L'élément $vin_id a bien été supprimé</p>"; 
             else if($results==1)
                 echo "<p>Un problème est survenu, Il est probable que l'élément $vin_id soit dans la table récolte</p>";
             else
                 echo "<p>Un problème est survenu, l'élément $vin_id n'a pas pu être supprimer</p>";
             ?>
          </div>
    </div>

  <?php include $root . '/app/view/fragment/fragmentCaveFooter.html'; ?>

  <!-- ----- fin viewId -->