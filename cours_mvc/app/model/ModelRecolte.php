
<!-- ----- debut ModelRecolte -->
<?php
require_once 'Model.php';

class ModelRecolte {

 private $producteur_id, $recolte_producteur_id, $quantite;

 // pas possible d'avoir 2 constructeurs
 public function __construct($producteur_id = NULL, $recolte_id = NULL, $quantite = NULL) {
  // valeurs nulles si pas de passage de parametres
  if (!is_null($producteur_id)) {
   $this->producteur_id = $producteur_id;
   $this->recolte_id = $recolte_id;
   $this->quantite = $quantite;
  }
 }

 function setProducteur_id($producteur_id) {
  $this->producteur_id = $producteur_id;
 }

 function setRecolte_id($recolte_id) {
  $this->recolte_id = $recolte_id;
 }

 function setQuantite($quantite) {
  $this->quantite = $quantite;
 }

 
 function getProducteur_id() {
  return $this->producteur_id;
 }

 function getRecolte_id() {
  return $this->recolte_id;
 }

 function getQuantite() {
  return $this->quantite;
 }

 
 public function __toString() {
  return $this->producteur_id;
 }

 // Persistance .......


 public static function view() {
  printf("<tr><td>%d</td><td>%s</td><td>%d</td><td>%.00f</td></tr>", $this->getProducteur_id(), $this->getRecolte_id(), $this->getQuantite());
 }

// retourne une liste des producteur_id
 public static function getAllProducteur_id() {
  try {
   $database = Model::getInstance();
   $query = "select producteur_id from recolte";
   $statement = $database->prepare($query);
   $statement->execute();
   $results = $statement->fetchAll(PDO::FETCH_COLUMN, 0);
   return $results;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }

 public static function getMany($query) {
  try {
   $database = Model::getInstance();
   $statement = $database->prepare($query);
   $statement->execute();
   $results = $statement->fetchAll(PDO::FETCH_CLASS, "ModelRecolte");
   return $results;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }

 public static function getAll() {
  try {
   $database = Model::getInstance();
   $query = "select * from recolte";
   $statement = $database->prepare($query);
   $statement->execute();
   $results = $statement->fetchAll(PDO::FETCH_CLASS, "ModelRecolte");
   return $results;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }

 public static function getOne($producteur_id) {
  try {
   $database = Model::getInstance();
   $query = "select vin_id from recolte where producteur_id = :producteur_id";
   $statement = $database->prepare($query);
   $statement->execute([
     'producteur_id' => $producteur_id
   ]);
   $results = $statement->fetchAll(PDO::FETCH_CLASS, "ModelRecolte");
   return $results;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }

 public static function insert($recolte_id, $quantite, $degre) {
  try {
   $database = Model::getInstance();

   // recherche de la valeur de la clé = max(producteur_id) + 1
   $query = "select max(producteur_id) from recolte";
   $statement = $database->query($query);
   $tuple = $statement->fetch();
   $producteur_id = $tuple['0'];
   $producteur_id++;

   // ajout d'un nouveau tuple;
   $query = "insert into recolte value (:producteur_id, :recolte_id, :quantite, :degre)";
   $statement = $database->prepare($query);
   $statement->execute([
     'producteur_id' => $producteur_id,
     'recolte_id' => $recolte_id,
     'quantite' => $quantite,
     'degre' => $degre
   ]);
   return $producteur_id;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return -1;
  }
 }

 public static function update() {
  echo ("ModelRecolte : update() TODO ....");
  return null;
 }

 public static function delete() {
  echo ("ModelRecolte : delete() TODO ....");
  return null;
 }
public static function getProducteur_Vin($producteur_id) {
  try {
   $database = Model::getInstance();
   $query = "select * from vin where id = :id";
   $statement = $database->prepare($query);
   $statement->execute([
     'id' => $id
   ]);
   $results = $statement->fetchAll(PDO::FETCH_CLASS, "ModelVin");
   return $results;
  } catch (PDOException $e) {
   printf("%s - %s<p/>\n", $e->getCode(), $e->getMessage());
   return NULL;
  }
 }
}
?>
<!-- ----- fin ModelRecolte -->
